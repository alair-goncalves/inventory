<?php

namespace Petiko\Inventory\Exceptions;

/**
 * Class InvalidSupplierException.
 */
class InvalidSupplierException extends \Exception
{
}
