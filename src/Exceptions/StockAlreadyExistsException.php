<?php

namespace Petiko\Inventory\Exceptions;

/**
 * Class StockAlreadyExistsException.
 */
class StockAlreadyExistsException extends \Exception
{
}
