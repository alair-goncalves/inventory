<?php

namespace Petiko\Inventory\Exceptions;

/**
 * Class StockNotFoundException.
 */
class StockNotFoundException extends \Exception
{
}
