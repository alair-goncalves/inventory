<?php

namespace Petiko\Inventory\Exceptions;

/**
 * Class NotEnoughStockException.
 */
class NotEnoughStockException extends \Exception
{
}
