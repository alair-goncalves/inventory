<?php

namespace Petiko\Inventory\Exceptions;

/**
 * Class NoUserLoggedInException.
 */
class NoUserLoggedInException extends \Exception
{
}
