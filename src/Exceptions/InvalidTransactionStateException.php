<?php

namespace Petiko\Inventory\Exceptions;

/**
 * Class InvalidTransactionStateException.
 */
class InvalidTransactionStateException extends \Exception
{
}
