<?php

namespace Petiko\Inventory\Exceptions;

/**
 * Class InvalidItemException.
 */
class InvalidItemException extends \Exception
{
}
