<?php

namespace Petiko\Inventory\Exceptions;

/**
 * Class InvalidLocationException.
 */
class InvalidLocationException extends \Exception
{
}
