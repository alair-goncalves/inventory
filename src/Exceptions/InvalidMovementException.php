<?php

namespace Petiko\Inventory\Exceptions;

/**
 * Class InvalidMovementException.
 */
class InvalidMovementException extends \Exception
{
}
