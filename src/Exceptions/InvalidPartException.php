<?php

namespace Petiko\Inventory\Exceptions;

/**
 * Class InvalidPartException.
 */
class InvalidPartException extends \Exception
{
}
